package com.creardigital.mooi

import android.content.Intent
import android.graphics.Bitmap
import android.net.http.SslError
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.SslErrorHandler
import android.webkit.WebChromeClient
import android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.RelativeLayout
import im.delight.android.webview.AdvancedWebView
import java.util.*
import kotlin.concurrent.schedule

class MainActivity : AppCompatActivity(), AdvancedWebView.Listener {

    companion object{
        val TAG:String?  = MainActivity::class.simpleName
    }

    lateinit var webView: AdvancedWebView
    lateinit var splash: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webView = findViewById(R.id.webview) as AdvancedWebView
        splash = findViewById(R.id.splash) as RelativeLayout

        webView.setListener(this, this)
        webView.setWebViewClient(object: WebViewClient(){
            override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                handler?.proceed()
            }
        })

        splash.visibility = View.VISIBLE
        Timer("loadPage", false).schedule(2000) {
            runOnUiThread {
                webView.loadUrl(BuildConfig.MOOI_URL)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        webView.onResume()
    }

    override fun onPause() {
        webView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        webView.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if(!webView.onBackPressed()){
            return
        }
        super.onBackPressed()
    }

    override fun onPageFinished(url: String?) {
        splash.visibility = View.GONE
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        Log.d(TAG, "errorCode: $errorCode, description: $description, faiingUrl: $failingUrl")
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {
    }

    override fun onExternalPageRequest(url: String?) {
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
    }
}
